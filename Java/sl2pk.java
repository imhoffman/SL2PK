import java.io.*;
import java.nio.*;
import java.util.Formatter;
import java.awt.Color;
import com.github.plot.Plot;
import com.github.plot.Plot.Line;
 
public class sl2pk {

 public static void main(String[] args) {
  int i;

  if (args.length < 1) {
   System.out.println(" usage: java sl2pk file.sl2");
   System.exit(0);
  }
//  String filename = args[0];

  for ( i=0; i<args.length; i++ ) {
   sl2exe(args[i]);
  }
 
 }

// https://www.codingeek.com/java/io/data-streams-a-convinient-read-write-primitives-in-java/
 static void sl2exe(String f) {
  short[] hdr = new short[4];
  byte  i1, freqCode;
  byte[] buff = new byte[8192];
  short i2, blockSize=0, lastBlockSize, sensor, packetSize;
  int   i4, i, frameIndex, time1, easting, northing, lastLat=0, lastLon=0, numRec=0, readRec=0;
  float f4, upperLimit, lowerLimit, depthFeet=0, waterSpeed, tempC, landSpeed, rads, altitude=0, heading;
  double[] lat=new double[16384], lon=new double[16384], depth=new double[16384];
  double Re, pi, latDeg, lonDeg, tmp;

  Re = 6356752.3142;
  pi = 3.141592653589793238;

  String csvname = f;
  csvname = f.substring(0,f.length()-4)+".csv";
  File csvfile = new File(csvname);

  try (
   InputStream s = new FileInputStream(f);
   BufferedInputStream b = new BufferedInputStream(s);
   DataInputStream d = new DataInputStream(b);
   Formatter csv = new Formatter(new FileWriter(csvfile)); )
   {
    for ( i=0; i<hdr.length; i++ ) {
     hdr[i] = le.i2(d);
    }
    // unlike openstreetmap, use an 8-byte header a la SeeSea
    System.out.format("  8-byte header\n");
    System.out.format("%04X %04X %04X %04X\n", hdr[0],hdr[1],hdr[2],hdr[3]);
    System.out.format(" ^SL type   ^sensor type\n\n");
 
    System.out.format(" data being written to file: %s\n",csvname);

    // read blocks as per the fortran program 
    // only correcting the endianess of the ones (pun intended) that we care about
    while (true) {
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); 

     blockSize = le.i2(d);
     lastBlockSize = le.i2(d);
     sensor = le.i2(d);
     packetSize = le.i2(d);
     frameIndex = d.readInt();
     upperLimit = le.f4(d);
     lowerLimit = le.f4(d);

     i4 = d.readInt(); i4 = d.readInt(); i1 = d.readByte();
     freqCode = d.readByte();
     i1 = d.readByte(); i1 = d.readByte();

     i4 = d.readInt();
     depthFeet = le.f4(d);
     time1 = d.readInt();
     i2 = d.readShort(); i4 = d.readInt();
			
     i2 = d.readShort();

     d.read(buff, 0, 4); d.read(buff, 0, 4); d.read(buff, 0, 4); d.read(buff, 0, 4);
     i2 = d.readShort(); i2 = d.readShort();
     d.read(buff, 0, 4); d.read(buff, 0, 4);

     easting = le.i4(d);
     northing = le.i4(d);
     waterSpeed = le.f4(d);
     rads = le.f4(d);
     altitude = le.f4(d);
     heading = le.f4(d);

     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i4 = d.readInt();
     d.read(buff, 0, packetSize);

     readRec++;
     if ( (readRec%200)==0 ) {
      System.out.format(".");
     }

     // many lat,lon pairs are dulicated and depth is often trivially zero
     //     skip those records
     if ( lastLat!=northing && lastLon!=easting && depthFeet != 0.0 ) {
      lastLat = northing;
      lastLon = easting;
      tmp = (double)northing/Re;
      tmp = Math.exp(tmp);
      tmp = (2.*Math.atan(tmp)-pi/2.);
      latDeg = tmp*180./pi;
      lonDeg = (double)easting/Re*180./pi;
      lat[numRec] = latDeg;
      lon[numRec] = lonDeg;
      depth[numRec] = -depthFeet;
      csv.format(" %13.9f, %15.9f, %12.3f\n", lat[numRec], lon[numRec], depth[numRec]);
      numRec++;
     }
    }
  } catch (FileNotFoundException e) { e.printStackTrace(); 
  } catch (IOException e) { //     e.printStackTrace();
  }
  System.out.format("\n number of record blocks read: %6d\n number of record blocks kept: %6d\n",readRec,numRec);

  String pngname = f;
  pngname = f.substring(0,f.length()-4);

  plotr.p(numRec, lon, lat, depth, pngname, altitude);
 }
}

// must switch endianess
// a la https://wiki.sei.cmu.edu/confluence/display/java/FIO12-J.+Provide+methods+to+read+and+write+little-endian+data
class le {

 public static short i2(DataInputStream d) {
  int n = 2;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getShort(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return -1;
 }

 public static int i4(DataInputStream d) {
  int n = 4;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getInt(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return -1;
 }

 public static float f4(DataInputStream d) {
  int n = 4;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getFloat(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return (float)-1E+217;
 }
}

class plotr {

 public static void p(int k, double[] xarb, double[] yarb, double[] zarb, String name, float alti) {
  int i;
  double xMin=xarb[0], xMax=xarb[0], yMin=yarb[0], yMax=yarb[0], zMin=zarb[0], zMax=-1E8;
  double[] x=new double[k], y=new double[k], z=new double[k];

  for ( i=0; i<k; i++ ) {
   x[i] = xarb[i]; y[i] = yarb[i]; z[i] = zarb[i];
   if ( x[i] < xMin ) { xMin = x[i]; }
   if ( x[i] > xMax ) { xMax = x[i]; }
   if ( y[i] < yMin ) { yMin = y[i]; }
   if ( y[i] > yMax ) { yMax = y[i]; }
   if ( z[i] < zMin ) { zMin = z[i]; }
   if ( z[i] > zMax ) { zMax = z[i]; }
  }

  double s = Math.sqrt( (yMax-yMin)*(yMax-yMin) + (xMax-xMin)*(xMax-xMin)*Math.cos(xMax*3.14159/180.)*Math.cos(xMax*3.14159/180.) )*3.14159/180.*6400.00;
  String title = String.format("%s: lat %+4.1f, lon %+5.1f, alt %4.0fm; %d points span %4.1f km", name, y[0], x[0], alti/3.28084, k, s);

  Plot plot = Plot.plot(Plot.plotOpts().
        title(title).
		legend(Plot.LegendFormat.BOTTOM)).
        xAxis("Longitude (deg)", Plot.axisOpts().
		range(xMin, xMax)).
        yAxis("Depth (ft)", Plot.axisOpts().
		range(zMin, zMax)).
	series("depth as a function of longitude", Plot.data().
		xy(x, z),
		Plot.seriesOpts().
			line(Line.NONE).
			marker(Plot.Marker.CIRCLE).
			markerColor(Color.BLUE).
			markerSize(2)
			);
  try {
  plot.save(name, "png");
  } catch (IOException e) { /* nothing */ }
 }
}
// (c) 2018 Ian M. Hoffman
