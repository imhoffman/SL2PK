import java.io.*;
import java.nio.*;
import java.util.Formatter;
import java.awt.Color;
import plplot.core.*;
import static plplot.core.plplotjavacConstants.*;
 
public class sl2pkv2 {

 public static void main(String[] args) {
  int i;

  if (args.length < 1) {
   System.out.println(" usage: java sl2pk file.sl2");
   System.exit(0);
  }
//  String filename = args[0];

  for ( i=0; i<args.length; i++ ) {
   sl2exe(args[i]);
  }
 
 }

// https://www.codingeek.com/java/io/data-streams-a-convinient-read-write-primitives-in-java/
 static void sl2exe(String f) {
  short[] hdr = new short[4];
  byte  i1, freqCode;
  byte[] buff = new byte[8192];
  short i2, blockSize=0, lastBlockSize, sensor, packetSize;
  int   i4, i, frameIndex, time1, easting, northing, lastLat=0, lastLon=0, numRec=0, readRec=0;
  float f4, upperLimit, lowerLimit, depthFeet=0, waterSpeed, tempC, landSpeed, rads, altitude=0, heading;
  double[] lat=new double[16384], lon=new double[16384], depth=new double[16384];
  double Re, pi, latDeg, lonDeg, tmp;

  Re = 6356752.3142;
  pi = 3.141592653589793238;

  String csvname = f;
  csvname = f.substring(0,f.length()-4)+".csv";
  File csvfile = new File(csvname);

  try (
   InputStream s = new FileInputStream(f);
   BufferedInputStream b = new BufferedInputStream(s);
   DataInputStream d = new DataInputStream(b);
   Formatter csv = new Formatter(new FileWriter(csvfile)); )
   {
    for ( i=0; i<hdr.length; i++ ) {
     hdr[i] = le.i2(d);
    }
    // unlike openstreetmap, use an 8-byte header a la SeeSea
    System.out.format("  8-byte header\n");
    System.out.format("%04X %04X %04X %04X\n", hdr[0],hdr[1],hdr[2],hdr[3]);
    System.out.format(" ^SL type   ^sensor type\n\n");
 
    System.out.format(" data being written to file: %s\n",csvname);

    // read blocks as per the fortran program 
    // only correcting the endianess of the ones (pun intended) that we care about
    while (true) {
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); 

     blockSize = le.i2(d);
     lastBlockSize = le.i2(d);
     sensor = le.i2(d);
     packetSize = le.i2(d);
     frameIndex = d.readInt();
     upperLimit = le.f4(d);
     lowerLimit = le.f4(d);

     i4 = d.readInt(); i4 = d.readInt(); i1 = d.readByte();
     freqCode = d.readByte();
     i1 = d.readByte(); i1 = d.readByte();

     i4 = d.readInt();
     depthFeet = le.f4(d);
     time1 = d.readInt();
     i2 = d.readShort(); i4 = d.readInt();
			
     i2 = d.readShort();

     d.read(buff, 0, 4); d.read(buff, 0, 4); d.read(buff, 0, 4); d.read(buff, 0, 4);
     i2 = d.readShort(); i2 = d.readShort();
     d.read(buff, 0, 4); d.read(buff, 0, 4);

     easting = le.i4(d);
     northing = le.i4(d);
     waterSpeed = le.f4(d);
     rads = le.f4(d);
     altitude = le.f4(d);
     heading = le.f4(d);

     i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort(); i2 = d.readShort();
     i4 = d.readInt();
     d.read(buff, 0, packetSize);

     readRec++;
     if ( (readRec%200)==0 ) {
      System.out.format(".");
     }

     // many lat,lon pairs are dulicated and depth is often trivially zero
     //     skip those records
     if ( lastLat!=northing && lastLon!=easting && depthFeet != 0.0 ) {
      lastLat = northing;
      lastLon = easting;
      tmp = (double)northing/Re;
      tmp = Math.exp(tmp);
      tmp = (2.*Math.atan(tmp)-pi/2.);
      latDeg = tmp*180./pi;
      lonDeg = (double)easting/Re*180./pi;
      lat[numRec] = latDeg;
      lon[numRec] = lonDeg;
      depth[numRec] = -depthFeet;
      csv.format(" %13.9f, %15.9f, %12.3f\n", lat[numRec], lon[numRec], depth[numRec]);
      numRec++;
     }
    }
  } catch (FileNotFoundException e) { e.printStackTrace(); 
  } catch (IOException e) { //     e.printStackTrace();
  }
  System.out.format("\n number of record blocks read: %6d\n number of record blocks kept: %6d\n",readRec,numRec);

  String pngname = f;
  pngname = f.substring(0,f.length()-4)+".png";

  plotrv2.p(numRec, lon, lat, depth, pngname, altitude);
 }
}

// must switch endianess
// a la https://wiki.sei.cmu.edu/confluence/display/java/FIO12-J.+Provide+methods+to+read+and+write+little-endian+data
class le {

 public static short i2(DataInputStream d) {
  int n = 2;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getShort(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return -1;
 }

 public static int i4(DataInputStream d) {
  int n = 4;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getInt(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return -1;
 }

 public static float f4(DataInputStream d) {
  int n = 4;
  try {
   byte[] buffer = new byte[n];
   int bytesRead = d.read(buffer,0,n);
   if ( bytesRead != n ) {
     throw new IOException("Read problem");
   }
   return ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).getFloat(); 
  } catch (IOException e) {
     e.printStackTrace();
  }
  return (float)-1E+217;
 }
}

class plotrv2 {
 private static double deltax=0.0, deltay=0.0, xcorn=0.55, ycorn=0.55;
 private static boolean tall=true;
 private static PLStream pls = new PLStream();

 public static void p(int k, double[] xarb, double[] yarb, double[] zarb, String name, float alti) {
  int i, iZMin=0;
  double xMin=xarb[0], xMax=xarb[0], yMin=yarb[0], yMax=yarb[0], zMin=zarb[0], zMax=-1E8;
  double ledge, redge, bedge, tedge, fourc;
  double[] x=new double[k], y=new double[k], z=new double[k];
  String line;

  ledge = 0.05;
  redge = 0.95;
  bedge = 0.05;
  tedge = 0.90;
  fourc = 0.45;     // defined as a difference...need to fix
  for ( i=0; i<k; i++ ) {
   x[i] = xarb[i]; y[i] = yarb[i]; z[i] = zarb[i];
   if ( x[i] < xMin ) { xMin = x[i]; }
   if ( x[i] > xMax ) { xMax = x[i]; }
   if ( y[i] < yMin ) { yMin = y[i]; }
   if ( y[i] > yMax ) { yMax = y[i]; }
   if ( z[i] < zMin ) { zMin = z[i]; iZMin = i; }
   if ( z[i] > zMax ) { zMax = z[i]; }
  }

//  try {
//  plot.save(name, "png");
//  } catch (IOException e) { /* nothing */ }
  System.out.format("\n plot being written to file: %s\n",name);
  pls.setopt("dev", "png");
  pls.setopt("o", name);
  pls.setopt("dpi", "200x200");
  pls.scolbg(255,255,255);
  pls.scol0( 15, 0, 0, 0 );
  pls.init();
  pls.adv(0);

  squr(xMin,xMax,yMin,yMax,ledge,redge,bedge,tedge,fourc);
  double s = Math.sqrt( deltay*deltay + deltax*deltax )*3.14159/180.*6400.00;
  String title = String.format("%s: lat %+4.1f, lon %+5.1f, alt %4.0fm; %d points span %4.1f km", name, y[0], x[0], alti/3.28084, k, s);

//  plfont(1);
  pls.sfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
  pls.schr(0,1.00);
  pls.col0(15);
  pls.width(1);
 // lat--lon
  cmap1_init();
  pls.vpor( xcorn, redge, ycorn, tedge );
  pls.wind(xMin,xMax, yMin,yMax);
  pls.col0(1);
  pls.ssym(0,0.25);
  for ( i=0; i<k; i++ ) {
   pls.col1( ( z[i] - zMin )/( zMax-zMin ) );
   plpoin1(x[i], y[i], 2);
  }
//  pls.poin(x,y,2);
  pls.col0(13);
  pls.ssym(0,2.00);
  plpoin1(x[iZMin],y[iZMin],4);
  pls.ssym(0,1.00);
  pls.col0(15);
  pls.schr(0,0.75);
  scaleBar(xMin,xMax,yMin,yMax);
  pls.col0(15);
  pls.lsty(1);
  pls.schr(0,0.75);
  pls.mtex("t",0.95,0.5,0.5,"lon");
  pls.col0(1);
  pls.sfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_BOLD);
  pls.mtex("t",3.55,0.5,0.5,"path");
  pls.sfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
  pls.col0(15);
  pls.mtex("r",1.25,0.5,0.5,"lat");
  pls.width(1);
  pls.schr(0,1.25);
  boxr("xy");
 // lat--depth panel
  pls.vpor( ledge, xcorn, ycorn, tedge );
  pls.wind(zMin,zMax, yMin,yMax);
  pls.col0(9);
  pls.ssym(0,0.25);
  pls.poin(z,y,2);
  pls.ssym(0,1.00);
  pls.col0(15);
  pls.lsty(1);
  pls.width(1);
  pls.schr(0,0.75);
  pls.mtex("l",0.95,0.5,0.5,"lat");
  boxr("yz");
  pls.col0(9);
  pls.lab("depth (ft)","","");
  pls.col0(15);
  pls.wind(zMin/3.28084,zMax/3.28084, yMin,yMax);
  pls.box( "cmts", 0.0, 0, "", 0.0, 0 );
  pls.col0(9);
  pls.mtex("t",3.55,0.5,0.5,"depth (m)");
  pls.col0(15);
 // lon--depth panel
  pls.vpor( xcorn, redge, bedge, ycorn );
  pls.wind(xMin,xMax, zMin,zMax);
  pls.col0(9);
  pls.ssym(0,0.25);
  pls.poin(x,z,2);
  pls.ssym(0,1.00);
  pls.col0(15);
  pls.lsty(1);
  pls.width(1);
  pls.schr(0,0.75);
  pls.mtex("b",1.25,0.5,0.5,"lon");
  boxr("xz");
  pls.wind(xMin,xMax, zMin/3.28084,zMax/3.28084);
  pls.box( "", 0.0, 0, "cmts", 0.0, 0 );
 // label panel
 // write some characteristic values to the plot
 // as diagnostic information for the user
  pls.vpor( ledge, xcorn, bedge, ycorn );
  pls.wind(0.0,100.0, 0.0,100.0);
  pls.col0(15);
  pls.lsty(1);
  pls.width(1);
  pls.schr(0,0.75);
  pls.sfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_BOLD);
  pls.ptex(01.0, 65.0, 1.0, 0.0, 0.0, name);
  pls.sfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
  line = String.format("%6d unique lat,lon span %3.1f km", k, Math.sqrt(deltax*deltax+deltay*deltay)*3.14159/180.*6400.0);
  pls.ptex(01.0, 50.0, 1.0, 0.0, 0.0, line);
  line = String.format("lat: %+5.1f,  lon: %+6.1f",y[iZMin],x[iZMin]);
  pls.ptex(01.0, 35.0, 1.0, 0.0, 0.0, line);
  line = String.format("   altitude (ft): %7.1f",alti);
  pls.ptex(01.0, 20.0, 1.0, 0.0, 0.0, line);

  pls.end();
 }

// arrange the plot panels sensibly after faithfully
// representing the boat track
 static void squr(double xmin,double xmax,double ymin,double ymax,
		double ledge,double redge,double bedge,double tedge,double fourc) {
  deltax = Math.abs( (xmax-xmin)*Math.cos(xmax*3.14159/180.) );
  deltay = Math.abs(ymax-ymin);
  if ( deltax > deltay ) {
   xcorn = redge - fourc;
   ycorn = tedge - (tedge - fourc)* deltay/ deltax;
   tall = false;
  } else {
   ycorn = tedge - fourc;
   xcorn = redge - (redge - fourc)* deltax/ deltay;
   tall = true;
  }
  return;
 }

// draw the appropriate axes depending on whether latitude
// or longitude is determining the arrangement
 static void boxr(String str) {
  if ( str == "xy" ) {
   if (tall) {
    pls.box( "bcts", deltay/2.0, 5, "bcts", deltay/2.0, 5 );
   } else {
    pls.box( "bcts", deltax/2.0, 5, "bcts", deltax/2.0, 5 );
   }
  } else if ( str == "xz" ) {
   if (tall) {
    pls.box( "bcts", deltay/2.0, 5, "bnvts", 0.0, 0 );
   } else {
    pls.box( "bcts", deltax/2.0, 5, "bnvts", 0.0, 0 );
   }
  } else if ( str == "yz" ) {
   if (tall) {
    pls.box( "bnts", 0.0, 0, "bcts", deltay/2.0, 5 );
   } else {
    pls.box( "bnts", 0.0, 0, "bcts", deltax/2.0, 5 );
   }
  } 
  return;
 }

 static void scaleBar(double xMin, double xMax, double yMin, double yMax) {
  String label;
  double[] x=new double[1], ylo=new double[1], yhi=new double[1];
  double y0=yMin+0.80*(yMax-yMin);
  double loglen=Math.floor(Math.log10( (yMax-yMin)/2.2*3.14159/180.*6.4E6 ));
  x[0]=xMin+0.80*(xMax-xMin);
  ylo[0]=y0-Math.pow(10,loglen)/2./6.4E6*180./3.14159;
  yhi[0]=y0+Math.pow(10,loglen)/2./6.4E6*180./3.14159;
  label = String.format("%.0fm",Math.abs(yhi[0]-ylo[0])*3.14159/180.*6.4E6);
  // color not changed here so that user has control in calling unit
  pls.erry(x,ylo,yhi);
  pls.ptex(xMin+0.78*(xMax-xMin),y0,1.0,0.0,1.0,label);
  return;
 }

 // pgpt1 didn't make it over from pgplot...
 static void plpoin1(double x, double y, int marker) {
  double[] xarr={x}, yarr={y};
  pls.poin(xarr,yarr,marker);
  return;
 }

 static void cmap1_init() {
  double i[] = new double[2];
  double h[] = new double[2];
  double l[] = new double[2];
  double s[] = new double[2];

  i[0] = 0.0;           // left boundary
  i[1] = 1.0;           // right boundary

  h[0] = 240;           // blue . green . yellow .
  h[1] = 0;             // . red

  l[0] = 0.6;
  l[1] = 0.6;

  s[0] = 0.8;
  s[1] = 0.8;

  pls.scmap1n( 256 );
  pls.scmap1l( false, i, h, l, s );
  return;
 }

}
// (c) 2018 Ian M. Hoffman
