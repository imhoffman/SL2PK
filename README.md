Standing on the shoulders of [Chris78](https://github.com/Chris78/sl2decode), [kmpm](https://github.com/kmpm/node-sl2format/blob/master/doc/sl2fileformat.md), [SeeSea](https://sourceforge.net/p/seesea/code/HEAD/tree/trunk/net.sf.seesea.navigation.sl2/), and [openstreetmap](https://wiki.openstreetmap.org/wiki/SL2), this is intended to be a utility for quickly inspecting a [Lowarance](https://www.lowrance.com/) .sl2 sonar file in the field before packing up.
Since the field laptop may be anything from a beater Windows system to a Chromebook to a Mac, we are writing a variety of programs in anticipation of being platform-limited.

This project is also an excuse to develop a cross-compiled, stand-alone executable in a variety of languages.

Here are sample plplot outputs from the C program for a single input file and for numerous input files.

![single](C/Sonar0012.png)
![multi](C/multi.png)

# Pre-compiled binaries
### Java
- [`SL2PK.jar`](Java/SL2PK.jar)
  - to be run from a command line as `java -jar SL2PK.jar file1.sl2 file2.sl2 ...`

### Windows
- [`SL2PK-Cygwin.zip`](C/SL2PK-Cygwin.zip)
  - unzip to produce the executable and several ancillary files&mdash;these files *must* stay together in the same directory with the .sl2 files
  - run from the Command Prompt as `sl2pk-cygwin.exe file1.sl2 file2.sl2 ...` to generate the .csv and a .png plot
  - depending on your security settings, the executable may need to be "Unblocked" by right-clicking in the file explorer
  - in addition to a .csv and a .png file for every input file, a single `multi.csv` file and a single `multi.png` plot will be generated that contains all of the input data
- [`SL2PK-Win32.zip`](C/SL2PK-Win32.zip)
  - unzip to produce the executable and four ancillary files&mdash;these files must stay together in the same directory with the .sl2 files
  - run from the Command Prompt as `sl2pk-Win32.exe file1.sl2 file2.sl2 ...` to generate the .csv and a .svg plot
  - depending on your security settings, the executable may need to be "Unblocked" by right-clicking in the file explorer
- [`sl22csv-Win32.exe`](C/sl22csv-Win32.exe)
  - to be run from the Command Prompt as `sl22csv-Win32.exe file1.sl2 file2.sl2 ...`

### ChromeOS
  - to be run from the `shell` in Developer Mode
- [`sl22csv-ChromeOS.bin`](chrome/sl22csv-ChromeOS.bin)
- [`sl2pk-ChromeOS.bin`](chrome/sl2pk-ChromeOS.bin)
  - for the full plotting program, also download [`plstnd5.fnt`](chrome/plstnd5.fnt) and [`plxtnd5.fnt`](chrome/plxtnd5.fnt)
  - set the shell variable using `$ export PLPLOT_LIB=.`

# Source Descriptions
## [`fortran`](fortran)

The [`sl22csv.f`](fortran/sl22csv.f) program will write out a comma-delimited plain text file with three columns: latitude, longitude, depth (negative).
The code is easily altered for different delimiters or different entries.
Licensed for use.

If you have [`pgplot`](http://www.astro.caltech.edu/~tjp/pgplot/) installed, then the full program [`sl2pk.f`](fortran/sl2pk.f) will provide a postscript plot in addition to the csv.
On the plot are various diagnostics including a proportioned map of the positions in the file, the depth as a function of latitude and longitude, and nominal values from the data set.
Bound by [the pgplot copyright](fortran/pgplot.txt).

## [`Java`](Java)

The [`sl2pk.java`](Java/sl2pk.java) program outputs a csv file with the same format as that of the fortran program.
There are three class files: the main `sl2pk`, `le` for little-endian conversions, and the `plotr`.
In addition to the csv, a rudimentary png file is generated using [simple Java plot](https://github.com/yuriy-g/simple-java-plot).
The Java program can accept multiple sl2 filenames and will output multiple csv and png files.
All of the necessary classes are available in the jar file [`SL2PK.jar`](Java/SL2PK.jar).
[Here is a link to](Java/SL2PK-Java-Instructions.pdf) instructions for installation and use on Windows.

The jar file [`SL2PKv2.jar`](Java/SL2PKv2.jar) has an improved PLPlot png output similar to the C program.
However, it has resisted my every effort to jar it up, I think because it [requires a system-dependent shared library](https://sourceforge.net/p/plplot/mailman/message/10283803/) that kind of defeats my goal of using the JVM for portability...

## [`C`](C)

The [`sl2pk.c`](C/sl2pk.c) program employs a data structure for the Lowrance block and outputs a csv file with the same format as the fortran and Java programs.
The C program can take multiple .sl2 files as inputs, outputting a separate csv file for each.
Plotting uses [`plplot`](http://plplot.sourceforge.net/index.php) and generates a png file similar to the fortran program.
Also available is [`sl22csv.c`](C/sl22csv.c) with only the csv as the output.
Licensed for use, including the plplot LGPL.

Both [the csv-only program](chrome/sl22csv-ChromeOS.bin) and [the full plotting program](chrome/sl2pk-ChromeOS.bin) have been compiled as static executables on a 64-bit Intel Chromebook using Chromebrew in Developer Mode.
Please email me if you try these on a Chromebook&mdash;I'd love to know how it goes!
(NB: The ChromeOS plotter outputs an svg rather than a png.)

#### disclaimer
This effort is in no way affiliated with Lowrance.
With the perspective gained from others' similar efforts, the binary format of the .sl2 files has been reverse-engineered and pressed into limited service.
Beware that the programs make dummy reads of *every* binary entry&mdash;these are intentional hooks for future development.
Nevertheless, there are far more reads than are needed for the few variables that are actually used; if performance is at a premium, then the code should be altered to read only the desired positions in the stream.
