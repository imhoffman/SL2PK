      program sl2pk
      implicit none
!
! program for reading Lowrance SL2 files
! outputs are (1) a space-delimited csv file
! and (2) a PostScript plot of the data
!
c
      integer nguess
      integer*4 values(13)
      character filename*32
c
      call parser(filename)
      call stat(filename, values)
      nguess = int(values(8)/1024)
!
! in order to allocate an appropriate amount of memory
! without running through a query subroutine, nguess
! is a conservative approximation of the number of records
! in the SL2 file. Rarely are records fewer than 1500 bytes,
! so assuming 1024 should be a suitable size-based guess.
!
c
      call sl2exe(filename,nguess)
c
      stop
      end
c
c  --== subroutines ==--
c
      subroutine sl2exe(f,nrec)
      implicit none
      character*(*) f
      integer nrec
c
      real lat(nrec), lon(nrec), depth(nrec)
      character outname*32, test*2, plcall*32
      integer i, ok, numrec, readrec, lnth, pos
      integer*1 i1, fcode
      integer*2 hdr(4),i2,bs,lbs,sensor,pcktsz
      integer*4 i4,findex,time1,east,north,lastlat,lastlon
      real*4    ulim,llim,dpthft,f4,wspeed,lspeed,rads,alt,headng
      double precision Re, pi, latdeg, londeg, tmp
c
      ok = 0
      numrec = 0
      readrec = 0
      Re = 6356752.3142D0
      pi = 3.14159265368979323846D0
      lastlat = 0
      lastlon = 0
c
c open file
      open (unit=11, file=f, err=1300,
     $         access='stream', !convert='BIG_ENDIAN',
     $         form='unformatted', status='old')
c
c read and display header
      do i = 1, size(hdr)         ! four two-byte entries
       read(11,err=1310) hdr(i)
      end do
      write(6,*) ' 8-byte header:'
      write(6,'(Z4.4,A1,Z4.4,A1,Z4.4,A1,Z4.4)')
     $      hdr(1),' ',hdr(2),' ',hdr(3),' ',hdr(4)
      write(6,*) '^SL type   ^sensor type'
      write(6,*) ''
c
c open output csv
      i = 1
      lnth = 1
      test = 'aa'
      do while ( test .ne. '  ' )
       i = i + 1
       lnth = lnth + 1
       test = f(i:i+1)
      end do
      outname = f(1:lnth-5) // '.csv'         ! assemble name
      plcall = f(1:lnth-5) // '.ps/CPS'         ! assemble name
      open(unit=12,file=outname,status='new',err=1330)
      write(6,*) ' data being written to file: ',outname
c
c read records following header
!   the table at openstreetmap is wrong---these offsets
!   were found empirically
!   only altitude, depth, lat, and lon tested so far
!   the commented lines are what openstreetmap would recommend
      do while ( ok.eq.0 )
       read(11,err=1200,iostat=ok)
     $  i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,
     $  bs,lbs,sensor,pcktsz,findex,ulim,llim,i4,i4,i1,
c     $  fcode,i2,time1,dpthft,i4,i2,i4,i2,f4,f4,f4,f4,i2,i2,
     $  fcode,i2,i4,dpthft,time1,i2,i4,i2,f4,f4,f4,f4,
c     $  i2,i2,lspeed,tempC,east,north,wspeed,rads,alt,headng,i2,i4,i4
     $  i2,i2,i4,lspeed,east,north,wspeed,rads,alt,headng,i2,i2,i2,i2,i4
       inquire(11, POS=pos)  ! get stream position then jump over packet
       read(11,POS=pos+pcktsz-1,err=1200,iostat=ok) i1
       readrec = readrec + 1
       if ( mod(readrec,200).eq.0 ) write(6,'(A1)',advance='no') '.'
c
! many of the entries have the same lat and lon and
! so they are checked before writing a uselessy large
! csv file---perhaps inspect depths to see if they are
! also duplicated and averaging is valuable
! zero-depth records are also useless
       if ( lastlat.ne.north
     $        .and. lastlon.ne.east
     $        .and. dpthft .ne.0.0 ) then
! converstion from integers to angular coordinates
! as per openstreetmap
        numrec = numrec + 1
        lastlat = north
        lastlon = east
        tmp = dble(north)/Re
        tmp = exp(tmp)
        tmp = (2.D0*atan(tmp)-pi/2.D0)
        latdeg = tmp*180.D0/pi
        londeg = dble(east)/Re*180.D0/pi
        lat(numrec) = real(latdeg)
        lon(numrec) = real(londeg)
        depth(numrec) = -dpthft
! write the line to the csv file
        write(12,'(F13.9,A1,F15.9,A1,F12.3)')
     $    lat(numrec),',', lon(numrec),',', depth(numrec)
       end if
! optional diagnostic prints for uncommenting
c       write(6,'(A15,Z4.4,I8)') ' block size: 0x',bs, bs
c       write(6,'(A15,Z8.8,I16)') '    easting: 0x',east, east
c       write(6,'(A15,Z8.8,I16)') '   northing: 0x',north, north
c       write(6,*) ' water depth in feet: ',dpthft
c       write(6,*) '    altitude in feet: ',alt
c       write(6,*) ' latitude in degs   : ',latdeg
c       write(6,*) ' longitude in degs  : ',londeg
      end do
      write(6,*) ''
      write(6,*) 'number of record blocks read: ', readrec
      write(6,*) 'number of record blocks kept: ', numrec
! plotting called from here so that the smaller, non-duplicate
! array need not be returned
      call plotter(numrec,lon,lat,depth,f,alt,plcall)
c
      goto 1350
1200  continue
      write(6,*) 'error in record read'
      goto 1410
1300  continue
      write(6,*) 'error in sl2 open'
      goto 1410
1310  continue
      write(6,*) 'error in header read'
      goto 1410
1330  write(6,*) 'output csv file already exists'
      goto 1410
1350  close(11)
      close(12)
1400  continue
      return
1410  close(11)
      close(12)
      stop
      end subroutine sl2exe
c
c
      subroutine parser(flnm)
      implicit none
      character flnm*32
c
      if ( iargc() .lt. 1 ) goto 1500
      call getarg(1, flnm)
      return
1500  write(6,*) ' usage: sl2pk file.sl2'
      stop
      end subroutine parser
c
c --== plotting ==--
c
      subroutine plotter(k,x,y,z,filename,altitude,plcall)
      implicit none
      integer k
      real x(k), y(k), z(k), altitude
      character filename*32, plcall*32
      integer i, pgbeg
      logical tall
      real xmin, xmax, ymin, ymax, zmin, zmax,
     $      deltax, deltay, xcorn, ycorn,
     $      ledge, bedge, redge, tedge, fourc
      character line*80
      real secify
c
c find extrema
      xmin = x(1)
      xmax = x(1)
      ymin = y(1)
      ymax = y(1)
      zmin = z(1)
      zmax = -50000.      ! don't plot depths of zero
      do i = 2, k
       if ( x(i).lt.xmin ) xmin = x(i)
       if ( x(i).gt.xmax ) xmax = x(i)
       if ( y(i).lt.ymin ) ymin = y(i)
       if ( y(i).gt.ymax ) ymax = y(i)
       if ( z(i).lt.zmin ) zmin = z(i)
       if ( z(i).gt.zmax .and. z(i).lt.0. ) zmax = z(i)
      end do
c pgplot preamble
      ledge = 0.05
      redge = 0.95
      bedge = 0.05
      tedge = 0.95
      fourc = 0.45      ! defined as a difference...need to fix
      i = 1
      do while ( plcall(i:i+1) .ne. 'ps' )
       i = i + 1
      end do
c      if ( pgbeg(0,'out.ps/CPS',1,1) .ne. 1 ) stop
      if ( pgbeg(0,plcall,1,1) .ne. 1 ) stop
      write(6,*) 'plots being written to file: ', plcall(1:i+1)
c      if ( pgbeg(0,'out.ps/VCPS',1,1) .ne. 1 ) stop
      call pgslw(3)
! three panels to be plotted
! based on the two-dimensional layout of the track
! along the surface of the earth
      call squr(xmin,xmax,ymin,ymax,deltax,deltay,
     $          ledge,redge,bedge,tedge,fourc,xcorn,ycorn,tall)
c lat--lon panel
      call pgsvp( xcorn, redge, ycorn, tedge )
      call pgswin(xmin,xmax, ymin,ymax)
! tracks are too small for D M S labelling, but pgplot can do it
c      call pgswin(secify(xmin),secify(xmax), secify(ymin),secify(ymax))
c      call pgtbox( 'bcntsz', 0.0, 0, 'bcntsz', 0.0, 0 )
      call pgsci(2)
      call pgpt(k,x,y,3)
      call pgsci(1)
      call pgsls(1)
      call pgsch(0.75)
      call pgptxt(xmin+(xmax-xmin)/2.,ymax+0.05*(ymax-ymin),0.0,0.5,
     $  'lon' )
      call pgptxt(xmax+0.05*(xmax-xmin),ymin+(ymax-ymin)/2.,270.,0.5,
     $  'lat' )
      call pgslw(3)
      call pgsch(1.25)
      call boxr('xy',deltax,deltay,tall)
c lat--depth panel
      call pgsvp( ledge, xcorn, ycorn, tedge )
      call pgswin(zmin,zmax, ymin,ymax)
      call pgsci(4)
      call pgpt(k,z,y,2)
      call pgsci(1)
      call pgsls(1)
      call pgslw(3)
      call pgsch(1.00)
      call boxr('yz',deltax,deltay,tall)
      call pglab('depth (ft)','','')
c lon--depth panel
      call pgsvp( xcorn, redge, bedge, ycorn )
      call pgswin(xmin,xmax, zmin,zmax)
      call pgsci(4)
      call pgpt(k,x,z,2)
      call pgsci(1)
      call pgsls(1)
      call pgslw(3)
      call pgsch(1.00)
      call boxr('xz',deltax,deltay,tall)
c label panel
! write some characteristic values to the plot
! as diagnostic information for the user
      call pgsvp( ledge, xcorn, bedge, ycorn )
      call pgswin(0.0,100.0, 0.0,100.0)
      call pgsci(1)
      call pgsls(1)
      call pgslw(3)
      call pgsch(1.25)
      call pgtext(01.0, 65.0, filename)
      write(line,'(I6,A25,F3.1,A3)') k,' unique lat,lon spanning ',
     $ sqrt(deltax*deltax+deltay*deltay)*3.14159/180.*6400.0,' km'
      call pgtext(01.0, 50.0, line)
      write(line,'(A5,F5.1,A8,F6.1)')
     $   'lat: ',y(1),',  lon: ',x(1)
      call pgtext(01.0, 35.0, line)
      write(line,'(A18,F7.1)')
     $   '   altitude (ft): ',altitude
      call pgtext(01.0, 20.0, line)
c
      return
      end subroutine plotter
c
c
!  arrange the plot panels sensibly after faithfully
!  representing the boat track
      subroutine squr(xmin,xmax,ymin,ymax,dx,dy,
     $      ledge,redge,bedge,tedge,fourc,xcorn,ycorn,tall)
      logical tall
      real xmin,xmax,ymin,ymax,dx,dy,xcorn,ycorn,
     $      ledge,redge,bedge,tedge,fourc
      dx = abs( (xmax-xmin)*cos(xmax*3.14159/180.) )
      dy = abs(ymax-ymin)
      if ( dx .gt. dy ) then
       xcorn = redge - fourc
       ycorn = tedge - (tedge - fourc)*dy/dx
       tall = .false.
      else
       ycorn = tedge - fourc
       xcorn = redge - (redge - fourc)*dx/dy
       tall = .true.
      end if
      return
      end subroutine squr
c
c
!  draw the appropriate axes depending on whether latitude
!  or longitude is determining the arrangement
      subroutine boxr(str,deltax,deltay,tall)
      character str*2
      logical tall
      real deltax, deltay
      if ( str == 'xy' ) then
       if (tall) then
        call pgbox( 'bcts', deltay/2.0, 5, 'bcts', deltay/2.0, 5 )
       else
        call pgbox( 'bcts', deltax/2.0, 5, 'bcts', deltax/2.0, 5 )
       end if
      else if ( str == 'xz' ) then
       if (tall) then
        call pgbox( 'bcts', deltay/2.0, 5, 'bcmts', 0.0, 0 )
       else
        call pgbox( 'bcts', deltax/2.0, 5, 'bcmts', 0.0, 0 )
       end if
      else if ( str == 'yz' ) then
       if (tall) then
        call pgbox( 'bcnts', 0.0, 0, 'bcts', deltay/2.0, 5 )
       else
        call pgbox( 'bcnts', 0.0, 0, 'bcts', deltax/2.0, 5 )
       end if
      else
       write(6,*) 'bad plot box'
       stop
      end if
      return
      end subroutine boxr
c
c
c --==functions==--
c
!  for converting to seconds in case DMS labelling is desired
!  UNTESTED
      real function secify(g)
      real g
      if ( g .ge. 0. ) then
       secify = real( floor(g)*3600.
     $        + floor( ( g-floor(g) )*60.0 )
     $        + g - g*60.0-floor(g*60.0) )
      else
       secify = real( ceiling(g)*3600.
     $        - ceiling( ( g+ceiling(g) )*60.0 )
     $        - g + g*60.0+ceiling(g*60.0) )
      end if
      return
      end function secify
c (c) 2018 Ian M. Hoffman; see also pgplot copyright
