      program sl22csv
      implicit none
!
! program for reading Lowrance SL2 files
! outputs a space-delimited csv file
!
c
      character filename*32
c
      call parser(filename)
c
      call sl2exe(filename)
c
      stop
      end
c
c  --== subroutines ==--
c
      subroutine sl2exe(f)
      implicit none
      character*(*) f
c
      character outname*32, test*2
      integer i, ok, numrec, readrec, lnth, pos
      integer*1 i1, fcode
      integer*2 hdr(4),i2,bs,lbs,sensor,pcktsz
      integer*4 i4,findex,time1,east,north,lastlat,lastlon
      real*4    ulim,llim,dpthft,f4,wspeed,lspeed,rads,alt,headng
      double precision Re, pi, latdeg, londeg, tmp
c
      ok = 0
      numrec = 0
      readrec = 0
      Re = 6356752.3142D0
      pi = 3.14159265368979323846D0
      lastlat = 0
      lastlon = 0
c
c open file
      open (unit=11, file=f, err=1300,
     $         access='stream', !convert='BIG_ENDIAN',
     $         form='unformatted', status='old')
c
c read and display header
      do i = 1, size(hdr)         ! four two-byte entries
       read(11,err=1310) hdr(i)
      end do
      write(6,*) ' 8-byte header:'
      write(6,'(Z4.4,A1,Z4.4,A1,Z4.4,A1,Z4.4)')
     $      hdr(1),' ',hdr(2),' ',hdr(3),' ',hdr(4)
      write(6,*) '^SL type   ^sensor type'
      write(6,*) ''
c
c open output csv
      i = 1
      lnth = 1
      test = 'aa'
      do while ( test .ne. '  ' )
       i = i + 1
       lnth = lnth + 1
       test = f(i:i+1)
      end do
      outname = f(1:lnth-5) // '.csv'         ! assemble name
      open(unit=12,file=outname,status='new',err=1330)
      write(6,*) ' data being written to file: ',outname
c
c read records following header
!   the table at openstreetmap is wrong---these offsets
!   were found empirically
!   only altitude, depth, lat, and lon tested so far
!   the commented lines are what openstreetmap would recommend
      do while ( ok.eq.0 )
       read(11,err=1200,iostat=ok)
     $  i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,i2,
     $  bs,lbs,sensor,pcktsz,findex,ulim,llim,i4,i4,i1,
c     $  fcode,i2,time1,dpthft,i4,i2,i4,i2,f4,f4,f4,f4,i2,i2,
     $  fcode,i2,i4,dpthft,time1,i2,i4,i2,f4,f4,f4,f4,
c     $  i2,i2,lspeed,tempC,east,north,wspeed,rads,alt,headng,i2,i4,i4
     $  i2,i2,i4,lspeed,east,north,wspeed,rads,alt,headng,i2,i2,i2,i2,i4
       inquire(11, POS=pos)  ! get stream position then jump over packet
       read(11,POS=pos+pcktsz-1,err=1200,iostat=ok) i1
       readrec = readrec + 1
       if ( mod(readrec,200).eq.0 ) write(6,'(A1)',advance='no') '.'
c
! many of the entries have the same lat and lon and
! so they are checked before writing a uselessy large
! csv file---perhaps inspect depths to see if they are
! also duplicated and averaging is valuable
! zero-depth records are also useless
       if ( lastlat.ne.north
     $        .and. lastlon.ne.east
     $        .and. dpthft .ne.0.0 ) then
! converstion from integers to angular coordinates
! as per openstreetmap
        numrec = numrec + 1
        lastlat = north
        lastlon = east
        tmp = dble(north)/Re
        tmp = exp(tmp)
        tmp = (2.D0*atan(tmp)-pi/2.D0)
        latdeg = tmp*180.D0/pi
        londeg = dble(east)/Re*180.D0/pi
! write the line to the csv file
        write(12,'(F13.9,A1,F15.9,A1,F12.3)')
     $            latdeg,',', londeg,',', -dpthft
       end if
! optional diagnostic prints for uncommenting
c       write(6,'(A15,Z4.4,I8)') ' block size: 0x',bs, bs
c       write(6,'(A15,Z8.8,I16)') '    easting: 0x',east, east
c       write(6,'(A15,Z8.8,I16)') '   northing: 0x',north, north
c       write(6,*) ' water depth in feet: ',dpthft
c       write(6,*) '    altitude in feet: ',alt
c       write(6,*) ' latitude in degs   : ',latdeg
c       write(6,*) ' longitude in degs  : ',londeg
      end do
      write(6,*) ''
      write(6,*) 'number of record blocks read: ', readrec
      write(6,*) 'number of record blocks kept: ', numrec
c
      goto 1350
1200  continue
      write(6,*) 'error in record read'
      goto 1410
1300  continue
      write(6,*) 'error in sl2 open'
      goto 1410
1310  continue
      write(6,*) 'error in header read'
      goto 1410
1330  write(6,*) 'output csv file already exists'
      goto 1410
1350  close(11)
      close(12)
1400  continue
      return
1410  close(11)
      close(12)
      stop
      end subroutine sl2exe
c
c
      subroutine parser(flnm)
      implicit none
      character flnm*32
c
      if ( iargc() .lt. 1 ) goto 1500
      call getarg(1, flnm)
      return
1500  write(6,*) ' usage: sl22csv file.sl2'
      stop
      end subroutine parser
c (c) 2018 Ian M. Hoffman
