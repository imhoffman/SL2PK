chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('main.html', {
    id: 'MainWindowID',
    bounds: { 'width': 650, 'height': 550 }
  },

  function (createdWindow) {
//    var sl2pk = require('child_process');
//    sl2pk.exec('pwd', function callback(error, stdout, stderr){
//      var func_ret = stdout;
//    });
    var func_ret = "something that gets returned from exec";

    var win = createdWindow.contentWindow;
    win.onload = function () {
      win.document.querySelector('#content').innerHTML = '<p>Test text.</p>';
      win.document.querySelector('#content').innerHTML += '<p>More test text.</p>';
      win.document.querySelector('#content').innerHTML += '<p>'+func_ret+'</p>';
//      win.print();     // for an actual printer
    }
  }

  );

//  chrome.runtime.sendNativeMessage('sl2pk.exe', { text: "Sonar0002.sl2" });

  chrome.runtime.onSuspend.addListener(function() {
  // no clean-up tasks yet
  });
});
