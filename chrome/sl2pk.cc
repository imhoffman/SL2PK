#include<stdio.h> 
#include<stdlib.h> 
#include<stdbool.h> 
#include<string.h> 
#include<math.h>
#include<plplot/plplot.h>

// prototypes
void sl2exe(char *filename);
void plotr(int k, double xarb[], double yarb[], double zarb[], char *name, float alt);
void boxr(const char *str, double deltax, double deltay, bool tall);
void squr(double xmin,double xmax,double ymin,double ymax,
		double *dx,double *dy,
		double ledge,double redge,double bedge,double tedge,
		double fourc,double *xcorn,double *ycorn,bool *tall);
void scaleBar(double xMin, double xMax, double yMin, double yMax);
void plpoin1(double x, double y, int marker);
void cmap1_init(void);

// binary block definition
typedef struct {
     short a01,a02,a03,a04,a05,a06,a07,a08,a09,a10,a11,a12,a13,a14;
     short blockSize, lastBlockSize, sensor, packetSize;
     int   frameIndex;
     float upperLimit, lowerLimit;
     int   b01, b02;
     char  c01;
     char  freqCode;
     char  c02, c03;
     int   b03;
     float depthFeet;
     int   time1;
     short a15;
     int   b04;
     short a16;
     float f01, f02, f03, f04;
     short a17, a18;
     float f05;
     int   easting, northing;
     float waterSpeed, rads, altitude, heading;
     short a19, a20, a21, a22;
     int   b05;
} block;

// main
int main(int argc, char *argv[]) {
 int i;

 if ( argc < 2 ) {
   printf(" usage: sl2pk file1.sl2 file2.sl2 ...\n");
   return 1;
 }

 for ( i=1; i<argc; i++ ) {
  sl2exe(argv[i]);
 }
 
 return 0;
}

// reader subroutine
void sl2exe (char *filename) {
 FILE *f, *csv;
 char csvname[strlen(filename)];
 int i, ok=1, readRec=0, numRec=0, lastLat=0, lastLon=0;
 short hdr[4];
 block b;
 double lat[16384], lon[16384], depth[16384];
 double Re, pi, latDeg, lonDeg;

 Re = 6356752.3142;
 pi = 3.141592653589793238;

 // open binary file and prepare csv file name
 f = fopen(filename, "rb");
 memcpy(csvname,filename,strlen(filename)-3);
 csvname[strlen(filename)-3]='\0';
 strncat(csvname,"csv",3); 

 // read and display binary header
 for ( i=0; i<sizeof(hdr)/sizeof(hdr[0]); i++ ) {
  ok = fread(&hdr[i],2,1,f);
  if ( ok == 0 ) {
   printf("problem reading binary file\n");
   return;
  }
 }
 printf("\n  data being read from file: %s\n",filename);
 printf("  8-byte header\n");
 printf("%04x %04x %04x %04x\n",hdr[0],hdr[1],hdr[2],hdr[3]);
 printf(" ^SL type   ^sensor type\n\n");

 // subsequent binary reads go into the output, so open the csv file
 csv = fopen(csvname, "w");
 printf(" data being written to file: %s\n",csvname);

 // main reading loop
 while( ok ) {
  // the integers and floats in the beginning of the block are what we use
  ok = fread(&b,sizeof(block),1,f);
  // the signal data at the end of each record gets skipped
  fseek(f,b.packetSize,SEEK_CUR);

  // progress dots
  readRec++;
  if ( (readRec%200)==0 ) { printf("."); }

  // many lat,lon pairs are dulicated and depth is often trivially zero
  //     skip those, convert the others for keeping
  if ( lastLat!=b.northing && lastLon!=b.easting && b.depthFeet!=0.0 ) {
   lastLat = b.northing;
   lastLon = b.easting;
   latDeg = (2.*atan(exp((double)b.northing/Re))-pi/2.)*180./pi;
   lonDeg = (double)b.easting/Re*180./pi;
   lat[numRec] = latDeg;
   lon[numRec] = lonDeg;
   depth[numRec] = -b.depthFeet;
   fprintf(csv," %13.9f, %15.9f, %12.3f\n", lat[numRec], lon[numRec], depth[numRec]);
   numRec++;
  }

 }
 printf("\n number of record blocks read: %6d\n number of record blocks kept: %6d\n",readRec,numRec);
 fclose(f);
 fclose(csv);

 // call plotting on the tidy, smaller array
 //   also pass the last altitude read, for labelling only
 plotr(numRec, lon, lat, depth, filename, b.altitude); 

 return;
}

void plotr(int k, double xarb[], double yarb[], double zarb[], char *name, float alt) {
 char plotname[strlen(name)], line[48];
 int i, iZMin=0;
 bool tall;
 double xMin=xarb[0], xMax=xarb[0], yMin=yarb[0], yMax=yarb[0], zMin=zarb[0], zMax=-1E8;
 double x[k], y[k], z[k];
 double ledge, redge, bedge, tedge, fourc, xcorn, ycorn, deltax, deltay;

 // prepare output file name
 memcpy(plotname,name,strlen(name)-3);
 plotname[strlen(name)-3]='\0';

 for ( i=0; i<k; i++ ) {
  x[i] = xarb[i]; y[i] = yarb[i]; z[i] = zarb[i];
  if ( x[i] < xMin ) { xMin = x[i]; }
  if ( x[i] > xMax ) { xMax = x[i]; }
  if ( y[i] < yMin ) { yMin = y[i]; }
  if ( y[i] > yMax ) { yMax = y[i]; }
  if ( z[i] < zMin ) { zMin = z[i]; iZMin = i; }
  if ( z[i] > zMax ) { zMax = z[i]; }
 }
 deltax = xMax-xMin; deltay = yMax-yMin;

 // plplot preable
 ledge = 0.05;
 redge = 0.95;
 bedge = 0.05;
 tedge = 0.90;
 fourc = 0.45;     // defined as a difference...need to fix
 plsetopt("dev", "svg");
 strncat(plotname,"svg",3); 
 plsetopt("o", plotname);
 printf("\n plot being written to file: %s\n",plotname);
 plsetopt("dpi", "100x100");
 plscolbg(255,255,255);
 plscol0( 15, 0, 0, 0 );
 plinit();
 pladv(0);

 // plfont(1);
 plsfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
 plschr(0,1.00);
 plcol0(15);
 plwidth(1.0);
 // three data panels to be plotted
 // based on the two-dimensional layout of the
 // track along the surface of the earth
 // the depth is _not_ to scale with the lat--lon track
 // squr determines all salient plate scales
 squr(xMin,xMax,yMin,yMax,&deltax,&deltay,ledge,redge,bedge,tedge,fourc,&xcorn,&ycorn,&tall);
 // lat--lon panel
 cmap1_init();
 plvpor( xcorn, redge, ycorn, tedge );
 plwind(xMin,xMax, yMin,yMax);
 plcol0(1);
 plssym(0,0.50);
 for ( i=0; i<k; i++ ) {
  plcol1( ( z[i] - zMin )/( zMax-zMin ) );
  plpoin1(x[i], y[i], 2);
 }
// plpoin(k,x,y,2);
 plcol0(13);
 plssym(0,2.00);
 plpoin1(x[iZMin],y[iZMin],4);
 plssym(0,1.00);
 plcol0(15);
 plschr(0,0.75);
 scaleBar(xMin,xMax,yMin,yMax);
 plcol0(15);
 pllsty(1);
 plschr(0,0.75);
 plmtex("t",0.95,0.5,0.5,"lon");
 plcol0(1);
 plsfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_BOLD);
 plmtex("t",3.55,0.5,0.5,"path");
 plsfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
 plcol0(15);
 plmtex("r",1.25,0.5,0.5,"lat");
 plwidth(1.0);
 plschr(0,1.25);
 boxr("xy",deltax,deltay,tall);
 // lat--depth panel
 plvpor( ledge, xcorn, ycorn, tedge );
 plwind(zMin,zMax, yMin,yMax);
 plcol0(9);
 plssym(0,0.50);
 plpoin(k,z,y,2);
 plssym(0,1.00);
 plcol0(15);
 pllsty(1);
 plwidth(1.0);
 plschr(0,0.75);
 plmtex("l",0.95,0.5,0.5,"lat");
 boxr("yz",deltax,deltay,tall);
 plcol0(9);
 pllab("depth (ft)","","");
 plcol0(15);
 plwind(zMin/3.28084,zMax/3.28084, yMin,yMax);
 plbox( "cmts", 0.0, 0, "", 0.0, 0 );
 plcol0(9);
 plmtex("t",3.55,0.5,0.5,"depth (m)");
 plcol0(15);
 // lon--depth panel
 plvpor( xcorn, redge, bedge, ycorn );
 plwind(xMin,xMax, zMin,zMax);
 plcol0(9);
 plssym(0,0.50);
 plpoin(k,x,z,2);
 plssym(0,1.00);
 plcol0(15);
 pllsty(1);
 plwidth(1.0);
 plschr(0,0.75);
 plmtex("b",1.25,0.5,0.5,"lon");
 boxr("xz",deltax,deltay,tall);
 plwind(xMin,xMax, zMin/3.28084,zMax/3.28084);
 plbox( "", 0.0, 0, "cmts", 0.0, 0 );
 // label panel
 // write some characteristic values to the plot
 // as diagnostic information for the user
 plvpor( ledge, xcorn, bedge, ycorn );
 plwind(0.0,100.0, 0.0,100.0);
 plcol0(15);
 pllsty(1);
 plwidth(1.0);
 plschr(0,0.75);
 plsfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_BOLD);
 plptex(01.0, 65.0, 1.0, 0.0, 0.0, name);
 plsfont(PL_FCI_MONO,PL_FCI_UPRIGHT,PL_FCI_MEDIUM);
 sprintf(line,"%6d unique lat,lon span %3.1f km", k, sqrt(deltax*deltax+deltay*deltay)*3.14159/180.*6400.0);
 plptex(01.0, 50.0, 1.0, 0.0, 0.0, line);
 sprintf(line,"lat: %+5.1f,  lon: %+6.1f",y[iZMin],x[iZMin]);
 plptex(01.0, 35.0, 1.0, 0.0, 0.0, line);
 sprintf(line,"   altitude (ft): %7.1f",alt);
 plptex(01.0, 20.0, 1.0, 0.0, 0.0, line);

 plend1();    // crashes on multiple plots on ubuntu desktop? OK on raspian stretch
 plend();
 return;
}

// arrange the plot panels sensibly after faithfully
// representing the boat track
void squr(double xmin,double xmax,double ymin,double ymax,
		double *dx,double *dy,
		double ledge,double redge,double bedge,double tedge,
		double fourc,double *xcorn,double *ycorn,bool *tall) {
 *dx = fabs( (xmax-xmin)*cos(xmax*3.14159/180.) );
 *dy = fabs(ymax-ymin);
 if ( *dx > *dy ) {
  *xcorn = redge - fourc;
  *ycorn = tedge - (tedge - fourc)* *dy/ *dx;
  *tall = false;
 } else {
  *ycorn = tedge - fourc;
  *xcorn = redge - (redge - fourc)* *dx/ *dy;
  *tall = true;
 }
 return;
}

// draw the appropriate axes depending on whether latitude
// or longitude is determining the arrangement
void boxr(const char *str, double deltax, double deltay, bool tall) {
 if ( str == "xy" ) {
  if (tall) {
   plbox( "bcts", deltay/2.0, 5, "bcts", deltay/2.0, 5 );
  } else {
   plbox( "bcts", deltax/2.0, 5, "bcts", deltax/2.0, 5 );
  }
 } else if ( str == "xz" ) {
  if (tall) {
   plbox( "bcts", deltay/2.0, 5, "bnvts", 0.0, 0 );
  } else {
   plbox( "bcts", deltax/2.0, 5, "bnvts", 0.0, 0 );
  }
 } else if ( str == "yz" ) {
  if (tall) {
   plbox( "bnts", 0.0, 0, "bcts", deltay/2.0, 5 );
  } else {
   plbox( "bnts", 0.0, 0, "bcts", deltax/2.0, 5 );
  }
 } 
 return;
}

void scaleBar(double xMin, double xMax, double yMin, double yMax) {
 char label[6];
 double x[1], ylo[1], yhi[1];
 double y0=yMin+0.75*(yMax-yMin);
 double loglen=floor(log10( (yMax-yMin)/2.*3.14159/180.*6.4E6 ));
 x[0]=xMin+0.75*(xMax-xMin);
 ylo[0]=y0-pow(10,loglen)/2./6.4E6*180./3.14159;
 yhi[0]=y0+pow(10,loglen)/2./6.4E6*180./3.14159;
 sprintf(label,"%.0fm",fabs(yhi[0]-ylo[0])*3.14159/180.*6.4E6);
 // color not changed here so that user has control in calling unit
 plerry(1,x,ylo,yhi);
 plptex(xMin+0.72*(xMax-xMin),y0,1.0,0.0,1.0,label);
 return;
}

// pgpt1 didn't make it over from pgplot...
void plpoin1(double x, double y, int marker) {
 double xarr[]={x}, yarr[]={y};
 plpoin(1,xarr,yarr,marker);
 return;
}

//straight out of PLPlot example 21
void cmap1_init(void) {
 double i[2], h[2], l[2], s[2];

 i[0] = 0.0;           // left boundary
 i[1] = 1.0;           // right boundary

 h[0] = 240.0;           // blue . green . yellow .
 h[1] = 0.0;             // . red

 l[0] = 0.6;
 l[1] = 0.6;

 s[0] = 0.8;
 s[1] = 0.8;

 plscmap1n( 256 );
 c_plscmap1l( 0, 2, i, h, l, s, NULL );
 return;
}
// (c) 2018 Ian M. Hoffman
