#include<stdio.h> 
#include<stdlib.h> 
#include<stdbool.h> 
#include<string.h> 
#include<math.h>

// prototypes
void sl2exe(char *filename);

// binary block definition
typedef struct {
     short a01,a02,a03,a04,a05,a06,a07,a08,a09,a10,a11,a12,a13,a14;
     short blockSize, lastBlockSize, sensor, packetSize;
     int   frameIndex;
     float upperLimit, lowerLimit;
     int   b01, b02;
     char  c01;
     char  freqCode;
     char  c02, c03;
     int   b03;
     float depthFeet;
     int   time1;
     short a15;
     int   b04;
     short a16;
     float f01, f02, f03, f04;
     short a17, a18;
     float f05;
     int   easting, northing;
     float waterSpeed, rads, altitude, heading;
     short a19, a20, a21, a22;
     int   b05;
} block;

// main
int main(int argc, char *argv[]) {
 int i;

 if ( argc < 2 ) {
   printf(" usage: sl2pk file1.sl2 file2.sl2 ...\n");
   return 1;
 }

 for ( i=1; i<argc; i++ ) {
  sl2exe(argv[i]);
 }
 
 return 0;
}

// reader subroutine
void sl2exe (char *filename) {
 FILE *f, *csv;
 char csvname[strlen(filename)];
 int i, ok=1, readRec=0, numRec=0, lastLat=0, lastLon=0;
 short hdr[4];
 block b;
 double Re, pi, latDeg, lonDeg;

 Re = 6356752.3142;
 pi = 3.141592653589793238;

 f = fopen(filename, "rb");
 memcpy(csvname,filename,strlen(filename)-3);
 csvname[strlen(filename)-3]='\0';
 strncat(csvname,"csv",3); 

 for ( i=0; i<sizeof(hdr)/sizeof(hdr[0]); i++ ) {
  fread(&hdr[i],2,1,f);
 }
 printf("\n  data being read from file: %s\n",filename);
 printf("  8-byte header\n");
 printf("%04x %04x %04x %04x\n",hdr[0],hdr[1],hdr[2],hdr[3]);
 printf(" ^SL type   ^sensor type\n\n");

 csv = fopen(csvname, "w");
 printf(" data being written to file: %s\n",csvname);

 while( ok ) {
  ok = fread(&b,sizeof(block),1,f);
  fseek(f,b.packetSize,SEEK_CUR);

  readRec++;
  if ( (readRec%200)==0 ) { printf("."); }

  // many lat,lon pairs are dulicated and depth is often trivially zero
  //     skip those records
  if ( lastLat!=b.northing && lastLon!=b.easting && b.depthFeet!=0.0 ) {
   lastLat = b.northing;
   lastLon = b.easting;
   latDeg = (2.*atan(exp((double)b.northing/Re))-pi/2.)*180./pi;
   lonDeg = (double)b.easting/Re*180./pi;
   fprintf(csv," %13.9f, %15.9f, %12.3f\n", latDeg, lonDeg, -b.depthFeet);
   numRec++;
  }

 }
 printf("\n number of record blocks read: %6d\n number of record blocks kept: %6d\n",readRec,numRec);
 fclose(f);
 fclose(csv);
 return;
}
// (c) 2018 Ian M. Hoffman
